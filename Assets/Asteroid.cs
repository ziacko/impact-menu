﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroid : MonoBehaviour {

	public enum team_t
	{
		red,
		green,
		blue,
		/*black,
		purple,
		orange,*/
		last
	}

	public enum size_t
	{
		large,
		small
	}

	public size_t size;
	public team_t team;
    
	public static float minimumSpeed = 0.025f;
    public static float maximumSpeed = 0.1f;
    public static float currentSpeed = minimumSpeed;

    public static float speedBuildTime = 300.0f;
    public static float currentSpeedBuildTime = 0f;


	public Vector3 targetPosition;

	public float progress;
	float randomProgress;

	Vector3 startPosition;

	public delegate void DamageEvent(Asteroid asteroid);
	public static event DamageEvent onDamage;

	bool isPaused = false;

	Quaternion randomquat;

	public static float rotationSpeed = 50.0f;

	public GameObject target;

	public bool dying = false;

	//randomly select mesh on start.

	// Use this for initialization
	void Start () {
		progress = 0.0f;
		randomProgress = 0.0f;
		startPosition = transform.position;
		randomquat = Random.rotation;

        minimumSpeed = 0.025f;
        maximumSpeed = 0.1f;
        currentSpeed = minimumSpeed;
		speedBuildTime = 300.0f;
		currentSpeedBuildTime = 0f;
		rotationSpeed = 50f;

}
	
	// Update is called once per frame
	void Update () {
		if (!isPaused)
		{
			if (progress < 1.0f)
			{
				progress += Time.deltaTime * currentSpeed;
				randomProgress += Time.deltaTime * (currentSpeed * 2);

				if (progress >= 0.99f)
				{
					Explode();
				}
			}

			else
			{

            }

			transform.localPosition = Vector3.Lerp(startPosition, targetPosition, progress);
			transform.Rotate(randomquat.eulerAngles.normalized * (Time.deltaTime * rotationSpeed));
		}
	}

	public void Explode()
	{
		dying = true;
        
		if(onDamage != null)
		{
			onDamage(this);
		}
	}

	public void Pause(bool pause)
	{
		isPaused = pause;
	}



}
