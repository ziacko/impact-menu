﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

//add an event for when the player hits the car
//add an event for when the player changes lanes
//have a boost
//put a boost icon over the car
//add function to smoothly change lanes
//add nitro boosting that recharges
//when using nitro the car moves further from the camera, making dodging cars harder
//add speed lines effect

public class Car : MonoBehaviour {

	public enum lanes_t
	{
		FAR_LEFT,
		LEFT,
		RIGHT,
		FAR_RIGHT
	}
	public lanes_t currentLane;// = lanes_t.RIGHT;
	public bool changingLanes;// = true;


	Vector3 startPosition;
	public List<float> lanePositions;
	
	public bool boosting = false;
	public bool canBoost = true;

	public float maxBoostTime = 10.0f;
	public float currentBoost = 0.0f;

	public float boostRechargeTime = 5.0f;
	public float currentBoostRechargeTime = 0.0f;

	public float defaultMaxSpeed = 100.0f;
	public float boostMaxSpeed = 180.0f;

	//public uint maxHealth = 3;
	public uint currentHits;

    public float startBias = 0.1f;
	public float laneBias = 0.01f;
    public float boostBias = 1.75f;

    float collisionForce = 100f;

	RCC_CarControllerV3 carController;

	public delegate void GameOverEvent(float delayTime);
	public static event GameOverEvent onGameOver;

	public Camera carCam;

    public GameObject sparkPrefab;
    public GameObject explosionPrefab;

    public ParticleSystem smokeEffect;
    public ParticleSystem fireEffect;
    //public ParticleSystem explosionEffect;

    //also need the hood position
    public Vector3 hoodPosition;

    public AudioSource alarmSource;
    bool alarmBlaring = false;

	// Use this for initialization
	void Start () {
		RCC_CarControllerV3.onDamage += TakeDamage;
		carController = GetComponent<RCC_CarControllerV3>();

		//set the car's max default speed
		carController.maxspeed = defaultMaxSpeed;
		currentLane = lanes_t.LEFT;
		changingLanes = true;

		currentBoostRechargeTime = boostRechargeTime;
		currentHits = 0;
        //disable the effects in start
        smokeEffect.Stop(true);
        fireEffect.Stop(true);
        //explosionEffect.Stop(true);

        alarmSource = GetComponent<AudioSource>();

        carCam = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();

        //	Button boostButton = GameObject.FindWithTag("Boost").GetComponent<Button>();
        //	boostButton.onClick.AddListener(Boost);


    }
	
	// Update is called once per frame
	void Update () {

		//if the car has been clicked on then turn on boost. if applicable
		//else check if it's on the left or right of the car
		if(Input.GetMouseButtonDown(0))
		{
			CheckInput();
		}

		if(changingLanes)
		{
			//ok so the car position has to match the selected position within reason
			//Debug.Log(lanePositions[(int)currentLane].ToString());
			float difference = Mathf.Abs((lanePositions[(int)currentLane] - transform.position.x));
			//Debug.Log(difference.ToString());
			//left
			if (lanePositions[(int)currentLane] < transform.position.x)
			{
                carController.steerInput = -1.0f;
                if (carController.speed < 10)
                {
                    if(difference < startBias)
                    {
                        carController.steerInput = 0.0f;
                        changingLanes = false;
                    }
                }
				
				if(boosting)
				{
					if (difference < boostBias)
					{
						carController.steerInput = 0.0f;
						changingLanes = false;
					}
				}

				else
				{
					if (difference < laneBias)
					{
						carController.steerInput = 0.0f;
						changingLanes = false;
					}
				}
			}

			//right
			if(lanePositions[(int)currentLane] > transform.position.x)
			{
				carController.steerInput = 1.0f;

                if (carController.speed < 10)
                {
                    if (difference < startBias)
                    {
                        carController.steerInput = 0.0f;
                        changingLanes = false;
                    }
                }
                if (boosting)
				{
					if (difference < boostBias)
					{
						carController.steerInput = 0.0f;
						changingLanes = false;
					}
				}

				else
				{
					if (difference < laneBias)
					{
						carController.steerInput = 0.0f;
						changingLanes = false;
					}
				}
			}
		}

		//if boosting. run down the clock to recharge it
		if (boosting)
		{
			if (currentBoost < maxBoostTime)
			{
				currentBoost += Time.deltaTime;
			}

			else
			{
				ResetBoost();
			}
		}

		else
		{
			//while not boosting. recharge it
			if (currentBoostRechargeTime < boostRechargeTime)
			{
				currentBoostRechargeTime += Time.deltaTime;
			}

			else
			{
				//can boost now
				canBoost = true;
			}
		}

        //get the 
        

        if(carController.speed <= defaultMaxSpeed)
        {
            carCam.fieldOfView = 45f;
        }

        else
        {
            float speedPercent = (carController.speed  - 100f)/ (boostMaxSpeed - 100f);
            carCam.fieldOfView = Mathf.Lerp(45f, 90f, speedPercent);
            //Debug.Log(speedPercent.ToString());
        }
	}

	void TakeDamage(Collision collision)
	{
		
		//stop boosting and reset the timer.
		//if boosting
		if (boosting)
		{
			//toss the car to the side
			Debug.Log(collision.gameObject.name);

            TossCar(collision.gameObject);
            //also add a ripple effect here
            Ripple rip = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Ripple>();
            Camera camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            rip.EmitRipple(camera.WorldToScreenPoint(transform.position));
            ResetBoost();
        }
        else
        {
            GameObject sparks = Instantiate(sparkPrefab, transform.position, transform.rotation);

            currentHits++;
        }
        
        if(currentHits == 3 && !smokeEffect.isPlaying)
        {
            smokeEffect.Play(true);
        }

        else if(currentHits == 5 && !fireEffect.isPlaying && smokeEffect.isPlaying)
        {
            smokeEffect.Stop(true);
            fireEffect.Play(true);
            alarmSource = GetComponent<AudioSource>();
            if (!alarmBlaring)
            {
                alarmBlaring = true;
                alarmSource.Play();
                alarmSource.loop = true;
            }

        }

       /* switch(currentHealth)
        {
            case 2:
                {
                    //enable the smoke particle system. it should be off by default
                    
                    break;
                }

            case 1:
                {
                    //enable the fire and disable the smoke
                    
                    break;
                }

            case 0:
                {
                    fireEffect.Stop(true);
                    //activate the explosion effect

                    GameObject explosion = Instantiate(explosionPrefab, transform.position, transform.rotation);
                    if (onGameOver != null)
                    {
                        onGameOver(1f);
                    }
                    break;
                }
        }*/
	}

	public void Boost()
	{
		//if not already boosting
		if(!boosting)
		{
			boosting = true;
			canBoost = false;
			currentBoostRechargeTime = 0.0f;
			currentBoost = 0.0f;
			carController.maxspeed = boostMaxSpeed;
		}
	}

	private void ResetBoost()
	{
		boosting = false;
		currentBoostRechargeTime = 0.0f;
		currentBoost = 0.0f;
		canBoost = false;
		carController.maxspeed = defaultMaxSpeed;
	}

    void TossCar(GameObject civCar)
    {
        Rigidbody rigid;
        bool useParent = false;
        if(civCar.GetComponent<Rigidbody>() == null)
        {
            rigid = civCar.GetComponentInParent<Rigidbody>();
            useParent = true;
            if(rigid == null)
            {
                return;
            }
        }

        else
        {
            rigid = civCar.GetComponent<Rigidbody>();
        }
        
        //ok now that we know what lane the car is in, toss it to that side,
        //remove the collision systems. so they don't stop at anything


        rigid.mass = 1f;
        rigid.useGravity = true;
        rigid.constraints = 0;// RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
        //int rand = Random.Range(0, 2);
        //attach a constant force to the car
        ConstantForce force = rigid.gameObject.AddComponent<ConstantForce>();
        lanes_t civLane = rigid.GetComponent<HR_TrafficCar>().currentLane;
        switch(civLane)
        {
            //turn off the box colliders
            case lanes_t.FAR_LEFT:
            case lanes_t.LEFT:
                {
                    //rigid.AddForce((Vector3.left + Vector3.up) * collisionForce);
                    force.force = (Vector3.left + Vector3.up) * collisionForce;
                    break;
                }

            case lanes_t.FAR_RIGHT:
            case lanes_t.RIGHT:
                {
                    //rigid.AddForce((Vector3.right + Vector3.up) * collisionForce);
                    force.force = ((Vector3.right + Vector3.up) * collisionForce);
                    break;
                }
        }

        
        if (useParent)
        {
            //mark the car for death after 3 or 2 seconds
            civCar.transform.parent.GetComponent<HR_TrafficCar>().MarkForDeath();
            foreach(BoxCollider col in civCar.transform.parent.GetComponentsInChildren<BoxCollider>())
            {
                col.enabled = false;
            }

        }

        else
        {
            civCar.GetComponent<HR_TrafficCar>().MarkForDeath();
            foreach (BoxCollider col in civCar.GetComponentsInChildren<BoxCollider>())
            {
                col.enabled = false;
            }
        }
        
    }

	void CheckInput()
	{
		Ray mouseRay = carCam.ScreenPointToRay(Input.mousePosition);
        RaycastHit[] hits = Physics.RaycastAll(mouseRay);

        if (hits.Length > 0)
		{

            for (int hitIter = 0; hitIter < hits.Length; hitIter++)
            {

                //check if the first thing the raycast hit was a trigger volume. in that case toss the car
                if (hits[hitIter].collider.name == "PaintableBody")
                {
                    TossCar(hits[hitIter].collider.gameObject);
                    break;
                }

                Debug.Log(hits[hitIter].collider.name);
			if (hits[hitIter].collider.GetComponentInParent<Car>() != null)
			{
				//time to boost
				if (canBoost)
				{
					Boost();
				}
			}

			else
			{
                if (!changingLanes)
                {
                        
                    //see what side of the screen it landed on. 
                    switch (hits[hitIter].collider.name)
                    {
                        case "far left":
                            {
                                currentLane = lanes_t.FAR_LEFT;
                                changingLanes = true;
                                break;
                            }

                        case "left":
                            {
                                currentLane = lanes_t.LEFT;
                                changingLanes = true;
                                break;
                            }

                        case "right":
                            {
                                currentLane = lanes_t.RIGHT;
                                changingLanes = true;
                                break;
                            }

                        case "far right":
                            {
                                currentLane = lanes_t.FAR_RIGHT;
                                changingLanes = true;
                                break;
                            }
                    }
                }
			}

            }
		}
	}

	private void OnDisable()
	{
		RCC_CarControllerV3.onDamage -= TakeDamage;
	}
}
