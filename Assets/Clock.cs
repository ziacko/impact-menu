﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Clock : MonoBehaviour {

	Text text;
	int seconds;
	int minutes;
	public bool isTicking;
	float time;
	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
		isTicking = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(isTicking)
		{
			seconds = (int)time % 60;
			minutes = (int)time / 60;
			text.text = minutes.ToString() + ":" + seconds.ToString();
		}
	}

	public void SetTime(float time)
	{
		this.time = time;
	}
}
