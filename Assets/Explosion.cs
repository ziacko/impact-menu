﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

	public ParticleSystem particleSystem;
	public float particleTimer = 0.0f;
	public AudioClip sound;
	bool dying = false;
	//TextMesh text;
	//public uint score;

	// Use this for initialization
	void Start () {
		particleSystem = GetComponent<ParticleSystem>();
		//text = GetComponentInChildren<TextMesh>();
		//text.text = null;
	}
	
	// Update is called once per frame
	void Update () {

		if (sound != null)
		{
			if (sound.length - 0.5f > particleTimer)
			{
				particleTimer += Time.deltaTime;
				//text.transform.rotation = Camera.main.transform.rotation;
				//text.text = score.ToString();
			}

			else
			{
				dying = true;
			}
		}

		else
		{

			if (particleTimer < particleSystem.main.duration)
			{
				particleTimer += Time.deltaTime;
			}

			else
			{
				dying = true;
			}
		}

		if(dying)
		{
			Destroy(gameObject);
		}
	}
}
