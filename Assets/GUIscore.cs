﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIscore : MonoBehaviour {

	public Image	backImage;
	public Text		scoreText;
	
	// Use this for initialization
	void Start () {
		scoreText = GetComponent<Text>();
		backImage = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Hide(bool show)
	{
		scoreText.enabled = show;
		scoreText.enabled = show;
	}
}
