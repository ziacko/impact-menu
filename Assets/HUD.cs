﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Globalization;
public class HUD : MonoBehaviour {
	
	public float canvasDistance = 10.0f;
	public worldhandler_Soccer handler = null;
	//public GameObject

	//get both text boxes
	public Canvas menuCanvas;

	public Text redScore;
	public Image redImage;

	public Text greenScore;
	public Image greenImage;

	public Text blueScore;
	public Image blueImage;

	public Text flavorText;

	public RawImage goalImage;

	public GameObject winningTeam;

	public GameObject backToMain;

	//this holds both scoreboards.
	GameObject scoreBoard;

	// Use this for initialization
	void Start () {
		menuCanvas = GetComponent<Canvas>();

		redScore = GameObject.Find("Score Board/red score/Team").GetComponent<Text>();
		redImage = GameObject.Find("Score Board/red score").GetComponent<Image>();

		greenScore = GameObject.Find("Score Board/green score/Team").GetComponent<Text>();
		greenImage = GameObject.Find("Score Board/green score").GetComponent<Image>();

		blueScore = GameObject.Find("Score Board/blue score/Team").GetComponent<Text>();
		blueImage = GameObject.Find("Score Board/blue score").GetComponent<Image>();

		winningTeam = GameObject.Find("Win Team");
		winningTeam.SetActive(false);
		scoreBoard = GameObject.Find("Score Board");
		backToMain.SetActive(false);

		DisableHUD(false);
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void ShowWinner(worldhandler_Soccer.teamName_t name, bool show = false)
	{
		winningTeam.SetActive(show);
		winningTeam.GetComponentInChildren<Text>().text = "team " + name.ToString() + " has won! Congratulations";

		backToMain.SetActive(true);
	}

	public void ShowSuddenDeath(string text)
	{
		winningTeam.SetActive(true);
		winningTeam.GetComponentInChildren<Text>().text = text;
	}

	public void reset()
	{
		//splashImage.enabled = false;
		//team1Score.color = new Color(0.45f, 0.45f, 1.0f);
		//HideScores(true);
		DisableHUD(false);
		//scoreBoard.SetActive(false);
		winningTeam.SetActive(false);
		/*for (uint scoreIter = 0; scoreIter < 5; scoreIter++)
		{
			scoreLeft[scoreIter].color = Color.black;
			scoreRight[scoreIter].color = Color.black;
		}*/

		scoreBoard.SetActive(false);
	}

	public void SetActiveScores(worldhandler_Soccer.teamName_t activeTeam)
	{
		//change the color to a darker color
		switch (activeTeam)
		{
			case worldhandler_Soccer.teamName_t.red:
				{
					redImage.color = Color.red;
					greenImage.color = Color.green / 3;
					blueImage.color = Color.blue / 3;
					break;
				}

			case worldhandler_Soccer.teamName_t.green:
				{
					greenImage.color = Color.green * 0.75f;
					redImage.color = Color.red / 3;
					blueImage.color = Color.blue / 3;
					break;
				}

			case worldhandler_Soccer.teamName_t.blue:
				{
					redImage.color = Color.red / 3;
					greenImage.color = Color.green / 3;
					blueImage.color = Color.blue;
					break;
				}
		}
	}

	public void DisableHUD(bool enable)
	{
		scoreBoard.SetActive(enable);
	}

	public void Disable(bool disable)
	{
		DisableHUD(false);
	}

	public void UpdateScores(worldhandler_Soccer.team teamRed, worldhandler_Soccer.team teamGreen, worldhandler_Soccer.team teamBlue)
	{
		redScore.text = "Score: " + teamRed.totalScore;
		greenScore.text = "Score: " + teamGreen.totalScore;
		blueScore.text = "Score: " + teamBlue.totalScore;
	}
}
