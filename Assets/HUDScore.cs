﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDScore : MonoBehaviour {

	//get image and text. and methods for settings these
	Text text;
	Image image;

	// Use this for initialization
	void Start () {
		image = GetComponent<Image>();
		text = GetComponentInChildren<Text>();	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SetColor(board.teamName_t color)
	{
		image = GetComponent<Image>();
		switch (color)
		{
			case board.teamName_t.red:
				{
					image.color = Color.red;
					break;
				}

			case board.teamName_t.blue:
				{
					image.color = Color.blue;
					break;
				}

			case board.teamName_t.green:
				{
					image.color = new Color(0.0f, 0.5f, 0.0f);
					break;
				}
		}
	}

	public void SetScore(uint score)
	{
		text = GetComponentInChildren<Text>();
		text.text = score.ToString();
	}
}
