﻿//----------------------------------------------
//            Realistic Car Controller
//
// Copyright © 2015 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[AddComponentMenu("BoneCracker Games/Realistic Car Controller/UI/Dashboard Displayer")]
[RequireComponent (typeof(RCC_DashboardInputs))]
public class RCC_UIDashboardDisplay : MonoBehaviour {

	public HR_PlayerHandler handler;

	public Text score;
	public Text timeLeft;
	public Text boostRecharge;

	public Text speed;
	public Text distance;
	public Text highSpeed;
	public Text hits;
	public Slider bombSlider;

	//private Image BoostImage;
	//private Vector2 boostDefPos;

	private Image highSpeedImage;
	private Vector2 highSpeedDefPos;

	private Image HealthImage;
	private Vector2 healthDefPos;

	private Image timeAttackImage;

	private RectTransform bombRect;
	private Vector2 bombDefPos;

	private Car playerCar;

	private void Start()
	{
		
	}

	void Awake () {

		HR_GamePlayHandler.OnPlayerSpawned += OnPlayerSpawned;
		//BoostImage = boostRecharge.GetComponentInParent<Image>();
		//boostDefPos = BoostImage.rectTransform.anchoredPosition;
		highSpeedImage = highSpeed.GetComponentInParent<Image>();
		highSpeedDefPos = highSpeedImage.rectTransform.anchoredPosition;
		HealthImage = hits.GetComponentInParent<Image>();
		healthDefPos = HealthImage.rectTransform.anchoredPosition;
		timeAttackImage = timeLeft.GetComponentInParent<Image>();
		bombRect = bombSlider.GetComponent<RectTransform>();
		bombDefPos = bombRect.anchoredPosition;



		StartCoroutine("LateDisplay");
		//introCamera.onIntroCanvasEvent += LateDisplay;
	}

	void OnEnable(){

		StopAllCoroutines();
		StartCoroutine("LateDisplay");

	}

	public IEnumerator LateDisplay () {

		while(true){

			yield return new WaitForSeconds(.04f);

			score.text = handler.score.ToString("F0");
			speed.text = handler.speed.ToString("F0");
			distance.text = (handler.distance).ToString("F2");
			highSpeed.text = handler.highSpeedCurrent.ToString("F1");			
			timeLeft.text = handler.timeLeft.ToString("F1");

			playerCar = GameObject.FindGameObjectWithTag("Player").GetComponent<Car>();
			hits.text = playerCar.currentHits.ToString();

			if (!playerCar.boosting)
			{
				boostRecharge.text = ((playerCar.currentBoostRechargeTime / playerCar.boostRechargeTime) * 100.0f).ToString("n0") + " %"; //handler.combo.ToString();
			}

			else
			{
				boostRecharge.text = ((float)(playerCar.currentBoost / playerCar.maxBoostTime) * 100.0f).ToString("n0") + " %"; //handler.combo.ToString();
			}

			if(playerCar.canBoost)
			{
				boostRecharge.color = Color.green;
			}

			else
			{
				boostRecharge.color = Color.red;
			}

			//Debug.Log(boostRecharge.text.ToString());

			if(HR_GamePlayHandler.Instance.mode == HR_GamePlayHandler.Mode.Bomb)
				bombSlider.value = handler.bombHealth / 100f;

		}

	}

	void Update(){

		if(!handler)
			return;

		/*if(handler.combo > 1){
			BoostImage.rectTransform.anchoredPosition = Vector2.Lerp(BoostImage.rectTransform.anchoredPosition, boostDefPos, Time.deltaTime * 5f);
		}else{
			BoostImage.rectTransform.anchoredPosition = Vector2.Lerp(BoostImage.rectTransform.anchoredPosition, new Vector2(boostDefPos.x - 500, boostDefPos.y), Time.deltaTime * 5f);
		}*/

		if(handler.highSpeedCurrent > .1f){
			highSpeedImage.rectTransform.anchoredPosition = Vector2.Lerp(highSpeedImage.rectTransform.anchoredPosition, highSpeedDefPos, Time.deltaTime * 5f);
		}else{
			highSpeedImage.rectTransform.anchoredPosition = Vector2.Lerp(highSpeedImage.rectTransform.anchoredPosition, new Vector2(highSpeedDefPos.x + 500, highSpeedDefPos.y), Time.deltaTime * 5f);
		}

		if(HR_GamePlayHandler.Instance.mode == HR_GamePlayHandler.Mode.TimeAttack){
			if(!timeLeft.gameObject.activeSelf)
				timeAttackImage.gameObject.SetActive(true);
		}else{ 
			if(timeLeft.gameObject.activeSelf)
				timeAttackImage.gameObject.SetActive(false);
		}

		if(HR_GamePlayHandler.Instance.mode == HR_GamePlayHandler.Mode.Bomb){
			if(!bombSlider.gameObject.activeSelf)
				bombSlider.gameObject.SetActive(true);
		}else{ 
			if(bombSlider.gameObject.activeSelf)
				bombSlider.gameObject.SetActive(false);
		}

		if(handler.bombTriggered){
			bombRect.anchoredPosition = Vector2.Lerp(bombRect.anchoredPosition, bombDefPos, Time.deltaTime * 5f);
		}else{
			bombRect.anchoredPosition = Vector2.Lerp(bombRect.anchoredPosition, new Vector2(bombDefPos.x - 500, bombDefPos.y), Time.deltaTime * 5f);
		}

	}

	void OnPlayerSpawned(GameObject player){

		handler = player.GetComponent<HR_PlayerHandler>();

	}

}
