﻿using UnityEngine;
using System.Collections;

	[System.Serializable]
	public class HR_CBProperties : ScriptableObject {

	public static HR_CBProperties instance;
	public static HR_CBProperties Instance
	{
		get
		{
			if(instance == null)
				instance = Resources.Load("HR_Assets/HR_CBProperties") as HR_CBProperties;
			return instance;
		}

	}

	public bool showAdsOnMainMenu = true;
	public bool showAdsOnGameOver = true;
	public int showAdsOnEachGameOver = 3;

}
		

