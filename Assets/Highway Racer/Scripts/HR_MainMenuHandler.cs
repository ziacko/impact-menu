﻿//----------------------------------------------
//           	   Highway Racer
//
// Copyright © 2016 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class HR_MainMenuHandler : MonoBehaviour {

	[Header("Spawn Location Of The Cars")]
	public Transform carSpawnLocation;

	internal GameObject[] cars;
	internal int carIndex = 0;
	internal int modeIndex = 0;

	[Header("UI Menus")]
	public GameObject optionsMenu;
	public GameObject carSelectionMenu;
	public GameObject modsSelectionMenu;
	public GameObject sceneSelectionMenu;
	public GameObject coinsMenu;
	public GameObject creditsMenu;

	[Header("UI Loading Section")]
	private AsyncOperation async;
	public GameObject loadingScreen;
	public Slider loadingBar;
	
	internal RCC_CarControllerV3[] currentCarControllers;
	internal HR_ModApplier[] currentModAppliers;

	[Header("Other UIs")]
	public Text currency;
	public GameObject buyCarButton;
	public GameObject selectCarButton;
	public GameObject modCarPanel;
	public GameObject bestScoreOneWay;
	public GameObject bestScoreTwoWay;
	public GameObject bestScoreTimeLeft;
	public GameObject bestScoreBomb;

	void Awake(){

		Time.timeScale = 1;
		AudioListener.volume = 1;

		if(HR_HighwayRacerProperties.Instance.mainMenuClips != null && HR_HighwayRacerProperties.Instance.mainMenuClips.Length > 0)
			RCC_CreateAudioSource.NewAudioSource(gameObject, "Main Menu Soundtrack", 0f, 0f, 1f, HR_HighwayRacerProperties.Instance.mainMenuClips[UnityEngine.Random.Range(0, HR_HighwayRacerProperties.Instance.mainMenuClips.Length)], true, true, false);

		if(HR_HighwayRacerProperties.Instance._1MMoneyForTesting)
			PlayerPrefs.SetInt("Currency", 1000000);

		carIndex = PlayerPrefs.GetInt("SelectedPlayerCarIndex", 0);

		CreateCars();
		SpawnCar();

	}

	void Start(){

//		GameObject.FindObjectOfType<GoogleMobileAdsShower> ().ShowBanner ();
//		GameObject.FindObjectOfType<GoogleMobileAdsShower> ().ShowInterstital ();

	}

	void Update(){

		currency.text = PlayerPrefs.GetInt("Currency", 0).ToString("F0");

		if(async != null &&  !async.isDone){
			loadingBar.value = async.progress;
		}

	}

	void CreateCars(){

		cars = new GameObject[HR_PlayerCars.Instance.cars.Length];
		currentCarControllers = new RCC_CarControllerV3[cars.Length];
		currentModAppliers = new HR_ModApplier[cars.Length];
		
		for(int i = 0; i < cars.Length; i++){

			cars[i] = (GameObject)Instantiate(HR_PlayerCars.Instance.cars[i].playerCar);
			cars[i].GetComponent<RCC_CarControllerV3>().canControl = false;
			cars[i].GetComponent<RCC_CarControllerV3>().engineRunning = false;
			cars[i].SetActive(false);
			currentCarControllers[i] = cars[i].GetComponent<RCC_CarControllerV3>();
			currentModAppliers[i] = cars[i].GetComponent<HR_ModApplier>();
			
		}
		
	}

	public void SpawnCar () {

		if(HR_PlayerCars.Instance.cars[carIndex].price <= 0 || HR_PlayerCars.Instance.cars[carIndex].unlocked)
			PlayerPrefs.SetInt(cars[carIndex].name + "Owned", 1);

		if(PlayerPrefs.GetInt(cars[carIndex].name + "Owned") == 1){
			if(buyCarButton.GetComponentInChildren<Text>())
				buyCarButton.GetComponentInChildren<Text>().text = "";
			buyCarButton.SetActive(false);
			selectCarButton.SetActive(true);
			modCarPanel.SetActive(true);
		}else{
			selectCarButton.SetActive(false);
			buyCarButton.SetActive(true);
			modCarPanel.SetActive(false);
			if(buyCarButton.GetComponentInChildren<Text>())
				buyCarButton.GetComponentInChildren<Text>().text = "BUY FOR\n" +  HR_PlayerCars.Instance.cars[carIndex].price.ToString("F0");
		}
		
		for(int i = 0; i < cars.Length; i++){
			cars[i].SetActive(false);
			cars[i].transform.position = carSpawnLocation.position;
			cars[i].transform.rotation = carSpawnLocation.rotation;
		}
		
		cars[carIndex].SetActive(true);
		
	}

	public void BuyCar (){

		if(PlayerPrefs.GetInt("Currency") >= HR_PlayerCars.Instance.cars[carIndex].price)
			PlayerPrefs.SetInt("Currency", PlayerPrefs.GetInt("Currency") - HR_PlayerCars.Instance.cars[carIndex].price);
		else
			return;
		
		PlayerPrefs.SetInt(cars[carIndex].name + "Owned", 1);

		SpawnCar();
		
	}

	public void SelectCar (){
		
		PlayerPrefs.SetInt("SelectedPlayerCarIndex", carIndex);
		
	}
	
	public void PositiveCarIndex (){
		
		carIndex ++;
		
		if(carIndex >= cars.Length)
			carIndex = 0;
		
		SpawnCar();
		
	}
	
	public void NegativeCarIndex (){
		
		carIndex --;
		
		if(carIndex < 0)
			carIndex = cars.Length - 1;
		
		SpawnCar();
		
	}

	public void EnableMenu(GameObject activeMenu){

		if(activeMenu.activeSelf){
			activeMenu.SetActive(false);
			return;
		}

		optionsMenu.SetActive(false);
		carSelectionMenu.SetActive(false);
		modsSelectionMenu.SetActive(false);
		sceneSelectionMenu.SetActive(false);
		coinsMenu.SetActive(false);
		creditsMenu.SetActive(false);
		loadingScreen.SetActive(false);

		activeMenu.SetActive(true);

		if(activeMenu == modsSelectionMenu)
			BestScores();

	}

	public void SelectScene(int levelIndex){

		SelectCar();
		EnableMenu(loadingScreen);
		async = SceneManager.LoadSceneAsync(levelIndex);

	}

	public void SelectMode(int _modeIndex){
		
		PlayerPrefs.SetInt("SelectedModeIndex", _modeIndex);
		EnableMenu(sceneSelectionMenu);
		
	}

	public void BestScores(){

		bestScoreOneWay.GetComponentInChildren<Text>().text = "BEST SCORE\n" + PlayerPrefs.GetInt("bestScoreOneWay", 0);
		bestScoreTwoWay.GetComponentInChildren<Text>().text = "BEST SCORE\n" + PlayerPrefs.GetInt("bestScoreTwoWay", 0);
		bestScoreTimeLeft.GetComponentInChildren<Text>().text = "BEST SCORE\n" + PlayerPrefs.GetInt("bestScoreTimeAttack", 0);
		bestScoreBomb.GetComponentInChildren<Text>().text = "BEST SCORE\n" + PlayerPrefs.GetInt("bestScoreBomb", 0);

	}

	public void QuitGame(){
		
		Application.Quit();
		
	}

}
