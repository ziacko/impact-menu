//----------------------------------------------
//           	   Highway Racer
//
// Copyright © 2016 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using System.Collections;

public class HR_ModApplier : MonoBehaviour {
	
	private RCC_CarControllerV3 carController;

	internal Color bodyColor;
	public MeshRenderer bodyRenderer;
	
	internal GameObject selectedWheel;
	internal int wheelIndex;

	internal bool isSirenPurchased = false;
	internal bool isSirenAttached = false;

	internal GameObject attachedFrontSiren;
	internal GameObject attachedRearSiren;

	//---------------------------//

	private int _speedLevel = 0;
	public int speedLevel
	{
		get
		{
			return _speedLevel;
		}
		set
		{
			if(value <= 5)
				_speedLevel = value;
		}
	}

	private int _handlingLevel = 0;
	public int handlingLevel
	{
		get
		{
			return _handlingLevel;
		}
		set
		{
			if(value <= 5)
				_handlingLevel = value;
		}
	}

	private int _brakeLevel = 0;
	public int brakeLevel
	{
		get
		{
			return _brakeLevel;
		}
		set
		{
			if(value <= 5)
				_brakeLevel = value;
		}
	}
	
	private float defMaxSpeed;
	private float defHandling;
	private float defMaxBrake;

	public float maxUpgradeSpeed;
	public float maxUpgradeHandling;
	public float maxUpgradeBrake;

	void Awake () {

		carController = GetComponent<RCC_CarControllerV3>();

		defMaxSpeed = carController.maxspeed;
		defHandling = carController.highspeedsteerAngle;
		defMaxBrake = carController.brake;

		wheelIndex = PlayerPrefs.GetInt(transform.name + "SelectedWheel", 0);
		selectedWheel = HR_Wheels.Instance.wheels[wheelIndex].wheel;

		_speedLevel = PlayerPrefs.GetInt(transform.name + "SpeedLevel");
		_handlingLevel = PlayerPrefs.GetInt(transform.name + "HandlingLevel");
		_brakeLevel = PlayerPrefs.GetInt(transform.name + "BrakeLevel");

		bodyColor = PlayerPrefsX.GetColor(transform.name + "BodyColor", HR_HighwayRacerProperties.Instance._defaultBodyColor);
		isSirenPurchased = PlayerPrefsX.GetBool(transform.name + "Siren", false);
		isSirenAttached = PlayerPrefsX.GetBool(transform.name + "SirenAttached", false);

	}

	void OnEnable(){

		HR_ModificationUpgrade[] mods = GameObject.FindObjectsOfType<HR_ModificationUpgrade>();

		for (int i = 0; i < mods.Length; i++) {

			mods[i].applier = GetComponent<HR_ModApplier>();

		}
		
		UpdateStats();

	}

	public void UpdateStats (){

		carController.maxspeed = Mathf.Lerp(defMaxSpeed, maxUpgradeSpeed, _speedLevel / 5f);
		carController.highspeedsteerAngle = Mathf.Lerp(defHandling, maxUpgradeHandling, _handlingLevel / 5f);
		carController.brake = Mathf.Lerp(defMaxBrake, maxUpgradeBrake, _brakeLevel / 5f);
		if(bodyRenderer)
			bodyRenderer.sharedMaterial.color = bodyColor;
//		else
//			Debug.LogError("Missing Body Renderer On ModApllier Component");

		if(isSirenPurchased && !attachedFrontSiren){
			CreateSiren();
		}

		for (int i = 0; i < carController.allWheelTransforms.Count; i++){
			carController.allWheelTransforms[i].GetComponent<MeshFilter>().mesh = selectedWheel.GetComponent<MeshFilter>().sharedMesh;
			//carController.allWheelTransforms[i].GetComponent<MeshRenderer>().sharedMaterial = selectedWheel.GetComponent<MeshRenderer>().sharedMaterial;
			PlayerPrefs.SetInt(transform.name + "SelectedWheel", wheelIndex);
		}

		PlayerPrefs.SetInt(transform.name + "SpeedLevel", _speedLevel);
		PlayerPrefs.SetInt(transform.name + "HandlingLevel", _handlingLevel);
		PlayerPrefs.SetInt(transform.name + "BrakeLevel", _brakeLevel);
		PlayerPrefsX.SetColor(transform.name + "BodyColor", bodyColor);
		PlayerPrefsX.SetBool(transform.name + "Siren", isSirenPurchased);
	
	}

	void Update(){

		if(maxUpgradeSpeed < carController.maxspeed)
			maxUpgradeSpeed = carController.maxspeed;

		if(maxUpgradeHandling < carController.highspeedsteerAngle)
			maxUpgradeHandling = carController.highspeedsteerAngle;

		if(maxUpgradeBrake < carController.brake)
			maxUpgradeBrake = carController.brake;

	}

	void CreateSiren(){

		if (GetComponent<RCC_CarControllerV3> ().headLights.Length < 2 || GetComponent<RCC_CarControllerV3> ().brakeLights.Length < 2) {
			Debug.LogError ("Vehicle is missing headlights or brakelights! In order to use sirens, you need to create at least two headlights and two brakelights for your vehicles.");
			return;
		}
		
		attachedFrontSiren = new GameObject("Front Siren Lights");
		attachedRearSiren = new GameObject("Rear Siren Lights");

		attachedFrontSiren.transform.SetParent(GetComponent<RCC_CarControllerV3>().chassis.transform, false);
		attachedRearSiren.transform.SetParent(GetComponent<RCC_CarControllerV3>().chassis.transform, false);

		GameObject frontSiren = (GameObject)Instantiate(HR_HighwayRacerProperties.Instance.attachableSiren, GetComponent<RCC_CarControllerV3>().headLights[0].transform.position, transform.rotation);
		GameObject rearSiren = (GameObject)Instantiate(HR_HighwayRacerProperties.Instance.attachableSiren, GetComponent<RCC_CarControllerV3>().brakeLights[0].transform.position, transform.rotation * Quaternion.Euler(0f, 180f, 0f));

		MeshFilter[] frontSirenLights = frontSiren.GetComponentsInChildren<MeshFilter>();
		MeshFilter[] rearSirenLights = rearSiren.GetComponentsInChildren<MeshFilter>();

		//Front Siren Lights
		for (int i = 0; i < 2; i++) {

			frontSiren.transform.SetParent(attachedFrontSiren.transform, true);

			if(frontSirenLights[i].transform.localPosition.x >= 0f)
				frontSirenLights[i].transform.position = GetComponent<RCC_CarControllerV3>().headLights[i].transform.position + (transform.right * .25f);
			else
				frontSirenLights[i].transform.position = GetComponent<RCC_CarControllerV3>().headLights[i].transform.position - (transform.right * .25f);
			
		}

		//Rear Siren Lights
		for (int i = 0; i < 2; i++) {

			rearSiren.transform.SetParent(attachedRearSiren.transform, true);
			
			if(rearSirenLights[i].transform.localPosition.x >= 0f)
				rearSirenLights[i].transform.position = GetComponent<RCC_CarControllerV3>().brakeLights[i].transform.position + (transform.right * .25f);
			else
				rearSirenLights[i].transform.position = GetComponent<RCC_CarControllerV3>().brakeLights[i].transform.position - (transform.right * .25f);

		}

		if(!isSirenAttached){
			attachedFrontSiren.SetActive(false);
			attachedRearSiren.SetActive(false);
		}

	}

	public void ToggleSiren (){

		if(isSirenPurchased && attachedFrontSiren){

			isSirenAttached = !isSirenAttached;
			
			if(isSirenAttached){
				attachedFrontSiren.SetActive(true);
				attachedRearSiren.SetActive(true);
				PlayerPrefsX.SetBool(transform.name + "SirenAttached", true);
			}else{
				attachedFrontSiren.SetActive(false);
				attachedRearSiren.SetActive(false);
				PlayerPrefsX.SetBool(transform.name + "SirenAttached", false);
			}

		}

	}

}
