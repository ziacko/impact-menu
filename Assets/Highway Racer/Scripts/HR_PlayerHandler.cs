﻿//----------------------------------------------
//           	   Highway Racer
//
// Copyright © 2016 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

[RequireComponent (typeof(Rigidbody))]
[RequireComponent (typeof(RCC_CarControllerV3))]
[RequireComponent (typeof(HR_ModApplier))]
public class HR_PlayerHandler : MonoBehaviour {

	private RCC_CarControllerV3 carController;

	private bool gameOver = false;
	private bool gameStarted{get{return HR_GamePlayHandler.Instance.gameStarted;}}

    float gameOverTimer = 0.0f;
    float gameOverTime = 5.0f;

	internal float score;
	internal float timeLeft = 60f;
	
	internal float speed = 0f;
	internal float distance = 0f;
	internal float highSpeedCurrent = 0f;
	internal float highSpeedTotal = 0f;
	internal int health = 3;

	private int minimumSpeedForGainScore
	{
		get
		{
			return HR_HighwayRacerProperties.Instance._minimumSpeedForGainScore;
		}
	}
	private int minimumSpeedForHighSpeed
	{
		get
		{
			return HR_HighwayRacerProperties.Instance._minimumSpeedForHighSpeed;
		}
	}

	private Vector3 previousPosition;

	private string currentTrafficCarNameLeft;
	private string currentTrafficCarNameRight;

	private GameObject nearMissText;
	internal int nearMisses;

	internal float combo;

	internal bool bombTriggered = false;
	internal float bombHealth = 100f;
	
	void Awake () {

		Application.targetFrameRate = 60;

		if(!GameObject.FindObjectOfType<RCC_UIDashboardDisplay>()){
			enabled = false;
			return;
		}

		carController = GetComponentInParent<RCC_CarControllerV3>();
		nearMissText = (GameObject)Instantiate(Resources.Load("HR_Near Miss", typeof(GameObject)), Vector3.zero, Quaternion.identity);
		nearMissText.transform.SetParent(GameObject.FindObjectOfType<RCC_UIDashboardDisplay>().transform);
		nearMissText.transform.SetAsFirstSibling();
		nearMissText.SetActive(false);

		Car.onGameOver += OnGameOver;
	
	}

	void Update () {

		if(QualitySettings.vSyncCount != 0)
			QualitySettings.vSyncCount = 0;

		if(gameOver)
        {
            Time.timeScale = 1;
            if(Input.GetKeyDown(KeyCode.Return))
            {
                SceneManager.LoadScene("main menu");
            }

            /*if(gameOverTimer < gameOverTime)
            {
                gameOverTimer += Time.unscaledDeltaTime;
            }

            else
            {
                SceneManager.LoadScene("main menu");
            }*/
            Time.timeScale = 0;
            return;
        }

		speed = carController.speed;

		distance += Vector3.Distance(previousPosition, transform.position) / 1000f;
		previousPosition = transform.position;

		if(speed >= minimumSpeedForGainScore){
			score += carController.speed * (Time.deltaTime * .05f);
		}

		if(speed >= minimumSpeedForHighSpeed){
			highSpeedCurrent += Time.deltaTime;
			highSpeedTotal += Time.deltaTime;
		}else{
			highSpeedCurrent = 0f;
		}

		if(HR_GamePlayHandler.Instance.mode == HR_GamePlayHandler.Mode.TimeAttack){
			timeLeft -= Time.deltaTime;
			if(timeLeft < 0){
				timeLeft = 0;
				OnGameOver(0f);
			}
		}

		if(HR_GamePlayHandler.Instance.mode == HR_GamePlayHandler.Mode.Bomb){

			if(speed > 80f){
				if(!bombTriggered)
					bombTriggered = true;
				else
					bombHealth += Time.deltaTime * 5f;
			}else if(bombTriggered){
				bombHealth -= Time.deltaTime * 10f;
			}

			bombHealth = Mathf.Clamp(bombHealth, 0f, 100f);

			if(bombHealth <= 0f){
				GameObject explosion = (GameObject)Instantiate(HR_HighwayRacerProperties.Instance.explosionEffect, transform.position, transform.rotation);
				explosion.transform.SetParent(null);
				GetComponent<Rigidbody>().isKinematic = true;
				OnGameOver(2f);
			}
		}
	}

	void FixedUpdate(){

		if(!gameOver && gameStarted)
			CheckNearMiss();

	}

	void CheckNearMiss(){
		
		RaycastHit hit;

		Debug.DrawRay(carController.COM.position, (-transform.right * 2f), Color.white);
		Debug.DrawRay(carController.COM.position, (transform.right * 2f), Color.white);
		Debug.DrawRay(carController.COM.position, (transform.forward * 20f), Color.white);
		
		if(Physics.Raycast(carController.COM.position, (-transform.right), out hit, 2f, HR_HighwayRacerProperties.Instance.trafficCarsLayer) && !hit.collider.isTrigger){
			currentTrafficCarNameLeft = hit.transform.name;
		}else{
			
			if(currentTrafficCarNameLeft != null && speed > HR_HighwayRacerProperties.Instance._minimumSpeedForGainScore){
				
				nearMisses ++;

				RCC_CreateAudioSource.NewAudioSource(gameObject, HR_HighwayRacerProperties.Instance.nearMissAudioClip.name, 0f, 0f, 1f, HR_HighwayRacerProperties.Instance.nearMissAudioClip, false, true, true);
				
				nearMissText.SetActive(true);
				nearMissText.GetComponent<HR_Viewport>().leftSide = true;
				nearMissText.GetComponentInChildren<Animator>().Play(0);

				score += 100f * Mathf.Clamp(combo / 1.5f, 1f, 20f);
				nearMissText.GetComponentInChildren<Text>().text = "+ " + (100f * Mathf.Clamp(combo / 1.5f, 1f, 20f)).ToString("F0");
				
				currentTrafficCarNameLeft = null;
				
			}else{
				
				currentTrafficCarNameLeft = null;
				
			}
			
		}
		
		if(Physics.Raycast(carController.COM.position, (transform.right), out hit, 2f, HR_HighwayRacerProperties.Instance.trafficCarsLayer) && !hit.collider.isTrigger){
			currentTrafficCarNameRight = hit.transform.name;
		}else{
			
			if(currentTrafficCarNameRight != null && speed > HR_HighwayRacerProperties.Instance._minimumSpeedForGainScore){
				
				nearMisses ++;
				combo ++;
				/*comboTime = 0;
				if(maxCombo <= combo)
					maxCombo = combo;*/
				
				RCC_CreateAudioSource.NewAudioSource(gameObject, HR_HighwayRacerProperties.Instance.nearMissAudioClip.name, 0f, 0f, 1f, HR_HighwayRacerProperties.Instance.nearMissAudioClip, false, true, true);

				nearMissText.SetActive(true);
				nearMissText.GetComponent<HR_Viewport>().leftSide = false;
				nearMissText.GetComponentInChildren<Animator>().Play(0);

				score += 100f * Mathf.Clamp(combo / 1.5f, 1f, 20f);
				nearMissText.GetComponentInChildren<Text>().text = "+ " + (100f * Mathf.Clamp(combo / 1.5f, 1f, 20f)).ToString("F0");
				
				currentTrafficCarNameRight = null;
				
			}else{
				
				currentTrafficCarNameRight = null;
				
			}
			
		}

		if(Physics.Raycast(carController.COM.position, (transform.forward), out hit, 40f, HR_HighwayRacerProperties.Instance.trafficCarsLayer) && !hit.collider.isTrigger){

			if(carController.highBeamHeadLightsOn)
				hit.transform.SendMessage("ChangeLines");

		}
		
	}

	void OnCollisionEnter(Collision col){

		combo = 0;

		if(col.relativeVelocity.magnitude < HR_HighwayRacerProperties.Instance._minimumCollisionForGameOver || (1 << col.gameObject.layer) != HR_HighwayRacerProperties.Instance.trafficCarsLayer.value)
			return;

		if(HR_GamePlayHandler.Instance.mode == HR_GamePlayHandler.Mode.Bomb){
			bombHealth -= col.relativeVelocity.magnitude / 2f;
			return;
		}

		//GetComponent<Rigidbody>().isKinematic = true;
		//OnGameOver(1f);

	}

	void OnGameOver(float delayTime){

		gameOver = true;
		GetComponent<Rigidbody>().isKinematic = true;
		HR_GamePlayHandler.Instance.StartCoroutine("OnGameOver", delayTime);

	}

	private void OnDisable()
	{
		Car.onGameOver -= OnGameOver;
	}
}
