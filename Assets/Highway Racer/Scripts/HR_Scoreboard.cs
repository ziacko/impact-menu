//----------------------------------------------
//           	   Highway Racer
//
// Copyright © 2016 BoneCracker Games
// http://www.bonecrackergames.com
//
//----------------------------------------------

using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HR_Scoreboard : MonoBehaviour {

	[Header("UI Texts On Scoreboard")]
	public Text totalScore;
	public Text subTotalMoney;
	public Text totalMoney;

	public Text totalDistance;
	public Text totalNearMiss;
	public Text totalOverspeed;
	public Text totalOppositeDirection;

	public Text totalDistanceMoney;
	public Text totalNearMissMoney;
	public Text totalOverspeedMoney;
	public Text totalOppositeDirectionMoney;
	
	public int totalDistanceMoneyMP
	{
		get
		{
			return HR_HighwayRacerProperties.Instance._totalDistanceMoneyMP;
		}
	}
	public int totalNearMissMoneyMP{
		get
		{
			return HR_HighwayRacerProperties.Instance._totalNearMissMoneyMP;
		}
	}
	public int totalOverspeedMoneyMP{
		get
		{
			return HR_HighwayRacerProperties.Instance._totalOverspeedMoneyMP;
		}
	}
	public int totalOppositeDirectionMP{
		get
		{
			return HR_HighwayRacerProperties.Instance._totalOppositeDirectionMP;
		}
	}

	public void DisplayResults(){

		HR_PlayerHandler playerHandler = GameObject.FindObjectOfType<HR_PlayerHandler>();

		totalScore.text = Mathf.Floor(playerHandler.score).ToString("F0");
		totalDistance.text = (playerHandler.distance).ToString("F2");
		totalNearMiss.text = (playerHandler.nearMisses).ToString("F0");
		totalOverspeed.text = (playerHandler.highSpeedTotal).ToString("F1");

		//totalDistanceMoney.text = Mathf.Floor(playerHandler.distance * totalDistanceMoneyMP).ToString("F0");
		//totalNearMissMoney.text = Mathf.Floor(playerHandler.nearMisses * totalNearMissMoneyMP).ToString("F0");
		//totalOverspeedMoney.text = Mathf.Floor(playerHandler.highSpeedTotal * totalOverspeedMoneyMP).ToString("F0");
		//totalOppositeDirectionMoney.text = Mathf.Floor(playerHandler.opposideDirectionTotal * totalOppositeDirectionMP).ToString("F0");

		//totalMoney.text = (Mathf.Floor(playerHandler.distance * totalDistanceMoneyMP) + (playerHandler.nearMisses * totalNearMissMoneyMP) + Mathf.Floor(playerHandler.highSpeedTotal * totalOverspeedMoneyMP) + Mathf.Floor(playerHandler.opposideDirectionTotal * totalOppositeDirectionMP)).ToString("F0");
		PlayerPrefs.SetInt("Currency", PlayerPrefs.GetInt("Currency", 0) + Mathf.FloorToInt(Mathf.Floor(playerHandler.distance * totalDistanceMoneyMP) + (playerHandler.nearMisses * totalNearMissMoneyMP) + Mathf.Floor(playerHandler.highSpeedTotal * totalOverspeedMoneyMP)));

		gameObject.BroadcastMessage("Animate");
		//gameObject.BroadcastMessage("GetNumber");

	}

}
