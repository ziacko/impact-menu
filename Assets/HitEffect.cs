﻿using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.ImageEffects;
using UnityEngine;

public class HitEffect : MonoBehaviour {

	bool isActivated = false;
	float max = 10.0f;
	float buildRate = 100.0f;
	//set the duration
	//set the max
	//set the build and decay speed
	//restart? or make it incredibly fast?
	VignetteAndChromaticAberration aberration;
	// Use this for initialization
	void Start () {
		aberration = GetComponent<VignetteAndChromaticAberration>();
	}
	
	// Update is called once per frame
	void Update () {
		if(isActivated)
		{
			if (aberration.chromaticAberration < max)
			{
				aberration.chromaticAberration += (buildRate * Time.deltaTime);
			}

			else
			{
				isActivated = false;
			}
		}

		else
		{
			if (aberration.chromaticAberration > 0)
			{
				aberration.chromaticAberration -= (buildRate * Time.deltaTime);
			}
		}

	}

	public void Reset()
	{
		//aberration.chromaticAberration = 0.0f;
		isActivated = true;
	}
}
