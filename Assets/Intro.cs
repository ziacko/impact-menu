﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using System.Collections;

public class Intro : MonoBehaviour {

	//public MovieTexture movie;
	public VideoSource movie;
	public VideoPlayer player;
	//GameObject intro;

	// Use this for initialization
	void Start () {
		player = GetComponent<VideoPlayer>();
		//player.Play();
		//GetComponent<RawImage>().texture = movie as MovieTexture;

	}
	
	// Update is called once per frame
	void Update () {

	}

	public void HideIntro(bool hide)
	{
		gameObject.SetActive(hide);
	}
}
