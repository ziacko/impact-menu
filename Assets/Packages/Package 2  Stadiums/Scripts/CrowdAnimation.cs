﻿using UnityEngine;
using System.Collections;

public class CrowdAnimation : MonoBehaviour {
	public Texture2D[] textures;
    public float fps;

    public Material material;
    private int current;
	
	
	
	void Start () {
        material = GetComponent<Renderer>().sharedMaterial;

        current = 0;

		StartCoroutine(UpdateAnimation());
	}
	
	IEnumerator UpdateAnimation () {
		while (true) {
			current++;
			
			if (current >= textures.Length)
				current = 0;

            material.SetTexture("_MainTexture", textures[current]);
			
			yield return new WaitForSeconds(1/fps);
		}
	}
}
