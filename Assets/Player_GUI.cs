﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player_GUI : MonoBehaviour {

	// Use this for initialization

	//get the text
	//get the image in child. NOT the one on current node
	//be able to set health via manipulating scale
	//set score 
	Text		playerScore;
	Image		playerHealthBar;
	Image		backHealthBar;

	void Start () {
		playerScore = GetComponentInChildren<Text>();
		playerHealthBar = GetComponentsInChildren<Image>()[1];
		backHealthBar = GetComponent<Image>();

		//Hide(false);
	}
	
	// Update is called once per frame
	void Update () {
		//SetHealth(2);
		
	}

	public void SetHealth(uint health)
	{
		playerHealthBar.rectTransform.localScale = new Vector3(
			health * 0.2f, playerHealthBar.transform.localScale.y, playerHealthBar.transform.localScale.z);
	}

	public void SetScore(uint score)
	{
		playerScore = GetComponentInChildren<Text>();
		playerHealthBar = GetComponentsInChildren<Image>()[1];
		backHealthBar = GetComponent<Image>();
		playerScore.text = "Score: " + score.ToString();
	}

	public void Hide(bool hide)
	{
		playerScore.enabled = hide;
		playerHealthBar.enabled = hide;
		backHealthBar.enabled = hide;
	}
}
