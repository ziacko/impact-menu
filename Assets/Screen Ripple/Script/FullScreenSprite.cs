﻿using UnityEngine;

public class FullScreenSprite : MonoBehaviour
{
	private SpriteRenderer m_SprRdr;
	private Transform m_Trans;
    void Start ()
    {
		m_SprRdr = GetComponent<SpriteRenderer> ();
		m_Trans = GetComponent<Transform> ();
    }
	void Update ()
    {
		m_Trans.localScale = new Vector3 (1f, 1f, 1f);
		
		float width = m_SprRdr.sprite.bounds.size.x;
		float height = m_SprRdr.sprite.bounds.size.y;
		float worldScreenHeight = Camera.main.orthographicSize * 2f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;
		m_Trans.localScale = new Vector3 (worldScreenWidth / width, worldScreenHeight / height, 1f);
    }
}
