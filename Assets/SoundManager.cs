﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour {

	//this holds all the sounds used in the game
	public AudioClip GoalSound;
	public AudioClip BallKicked;
	public AudioClip defaultMusic;
	public AudioClip buttonSound;
	public AudioClip whistleSound;
	public AudioClip goalPostSound;
	public AudioClip goalSuccess;
	public AudioClip goalFail;
	public AudioClip[] crowdRandom;
	public AudioClip stadiumAmbient;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
