﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldHandler : MonoBehaviour {

	// Use this for initialization

	//hold the game time. games should last 2 or 3 minutes
	//scores are based on distance from start
	//extra points for near misses
	//hit to the left or right of the car to change lane

	public delegate void ChangeLaneEvent();
	public static event ChangeLaneEvent onChangeLane;

	public delegate void BoostEvent();
	public static event BoostEvent onBoost;


	public float gameTimeLimit = 120.0f;
	float currentGameTime = 0.0f;

	public enum gameState_t
	{
		INTRO,
		MENU,
		DEFAULT,
	}

	gameState_t currentGameState = gameState_t.INTRO;

	Car car;

	void Start () {

		//start the game at the intro
		//HR_PlayerHandler.onDamage += TakeDamage;
		car = GameObject.FindGameObjectWithTag("Player").GetComponent<Car>();

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	//reset the car position, reset the civilian cars
	private void Reset() 
	{

	}
}
