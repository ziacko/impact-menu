﻿using UnityEngine;
using System.Collections;

public class board : MonoBehaviour {

	//a board should have a player that it is connected to
	//use handler to actually use it?
	net childNet;

	public enum teamName_t
	{
		red,
		green,
		blue,
		none
	};

	public uint score;
	public teamName_t team;

	// Use this for initialization
	void Start () {
		childNet = GetComponentInChildren<net>();
		childNet.team = team;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
