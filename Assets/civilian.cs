﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class civilian : MonoBehaviour {

	//activate and deactivate 

	//set the animation to just idle and keep it there
	//deactivate gameobject
	Animator anim;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator>();
		anim.GetCurrentAnimatorStateInfo(0).IsName("Grounded");
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Hide(bool enable)
	{
		gameObject.SetActive(enable);
		GetComponentInChildren<SkinnedMeshRenderer>().enabled = enable;
	}
}
