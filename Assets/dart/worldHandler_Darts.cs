﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class worldHandler_Darts : MonoBehaviour {

	struct boardAngles_t
	{
		public boardAngles_t(float startAngle, float endAngle, int boardValue)
		{
			start = startAngle;
			end = endAngle;
			value = boardValue;
		}

		float start;
		float end;
		int value;
	}

	List<boardAngles_t> boardAngles;


	// Use this for initialization
	void Start () {
		boardAngles = new List<boardAngles_t>(20);

		boardAngles.Add(new boardAngles_t(351f, 9f, 20));
		boardAngles.Add(new boardAngles_t(9f, 27f, 1));
		boardAngles.Add(new boardAngles_t(27f, 45f, 18));
		boardAngles.Add(new boardAngles_t(45f, 63f, 4));
		boardAngles.Add(new boardAngles_t(63f, 81f, 13));
		boardAngles.Add(new boardAngles_t(81f, 109f, 6));
		boardAngles.Add(new boardAngles_t(109f, 127f, 10));
		boardAngles.Add(new boardAngles_t(127f, 145f, 15));
		boardAngles.Add(new boardAngles_t(145f, 163f, 2));
		boardAngles.Add(new boardAngles_t(163f, 181f, 17));
		boardAngles.Add(new boardAngles_t(181f, 199f, 3));
		boardAngles.Add(new boardAngles_t(199f, 217f, 19));
		boardAngles.Add(new boardAngles_t(217f, 235f, 7));
		boardAngles.Add(new boardAngles_t(235f, 253f, 16));
		boardAngles.Add(new boardAngles_t(253f, 271f, 8));
		boardAngles.Add(new boardAngles_t(271f, 289f, 11));
		boardAngles.Add(new boardAngles_t(289f, 307f, 14));
		boardAngles.Add(new boardAngles_t(307f, 325f, 9));
		boardAngles.Add(new boardAngles_t(325f, 333f, 12));
		boardAngles.Add(new boardAngles_t(333f, 351f, 5));
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
