﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class gameMenu : menuPanel {

	public Button mainMenu;
	public GameObject symbol;

	public Button oneTeam;
	public Button twoTeams;
	public Button threeTeams;
	public Button fourTeams;

	public string beginnerLevelToLoad;
	public string traineeLevelToLoad;
	public string proLevelToLoad;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnEnable()
	{
		BaseOnEnable(true);
	}

	private void OnDisable()
	{
		BaseOnEnable(false);
	}

	public override void BaseOnEnable(bool toEnable)
	{
		base.BaseOnEnable(toEnable);
		mainMenu.gameObject.SetActive(toEnable);
		if (symbol != null)
		{
			symbol.SetActive(toEnable);
		}

		if (oneTeam != null)
		{
			oneTeam.gameObject.SetActive(toEnable);
		}

		if (twoTeams != null)
		{
			twoTeams.gameObject.SetActive(toEnable);
		}

		if (threeTeams != null)
		{
			threeTeams.gameObject.SetActive(toEnable);
		}

		if (fourTeams != null)
		{
			fourTeams.gameObject.SetActive(toEnable);
		}
	}

	public void LoadNoviceLevel()
	{
		SceneManager.LoadScene(beginnerLevelToLoad);
	}

	public void LoadTraineeLevel()
	{
		SceneManager.LoadScene(traineeLevelToLoad);
	}

	public void LoadProLevel()
	{
		SceneManager.LoadScene(proLevelToLoad);
	}
}
