﻿using UnityEngine;
using System.Collections;

public class goalie : MonoBehaviour
{

	//make a list of animations
	public enum animationState
	{
		CHEER,
		HIGH_CENTER,
		HIGH_LEFT,
		HIGH_RIGHT,
		LOW_LEFT,
		LOW_RIGHT,
		MID_CENTER,
	};

	public worldhandler_Soccer handler = null;
	
	public float goalieSpeed;

	//bool jumping = false;

	Rigidbody body;
	public float upForce = 100.0f;
	public float goalieForce = 100.0f;
	public bool caughtBall = false;

	public float leapDelay = 0.5f;
	public float currentLeapDelay = 0.0f;

	Vector3 beginningPosition = new Vector3();

	public GameObject leftHand;
	public GameObject rightHand;

	Animator anim;

	//ok we need to know if the goalie has a predetermined animation
	public bool predetermined = false;
	public animationState determinedState = animationState.CHEER;

	// Use this for initialization
	void Start()
	{
		beginningPosition = transform.position;
		body = GetComponent<Rigidbody>() as Rigidbody;
		anim = GetComponent<Animator>();

		leftHand = GameObject.FindGameObjectWithTag("Left Hand");
		rightHand = GameObject.FindGameObjectWithTag("Right Hand");

		Reset();
		anim.SetTrigger("idle");
	}

	public void KeepBallInHands(soccerBall gameBall)
	{
		//disable physics components of ball and put it in between the hands
		Vector3 directionBtoA = leftHand.transform.position - rightHand.transform.position; // directionCtoA = positionA - positionC
		Vector3 directionAtoB = rightHand.transform.position - leftHand.transform.position; // directionCtoB = positionB - positionC
		Vector3 midpointAtoB = new Vector3((directionBtoA.x + directionAtoB.x) / 2.0f, (directionBtoA.y + directionAtoB.y) / 2.0f, (directionBtoA.z + directionAtoB.z) / 2.0f); // midpoint between A B this is what you want

		gameBall.transform.position = midpointAtoB;
	}

	public void Reset()
	{
		transform.position = beginningPosition;
	}

	public void SetAnimation(animationState newAnimation)
	{
		switch (newAnimation)
		{
			case animationState.HIGH_LEFT:
				{
					string name = "high left ";
					body.AddForce((transform.right * goalieForce) + (transform.up * upForce));
					anim.SetTrigger(name + Random.Range(1, 4).ToString());
					break;
				}

			case animationState.HIGH_RIGHT:
				{
					string name = "high right ";
					body.AddForce((-transform.right * goalieForce) + (transform.up * upForce));
					anim.SetTrigger(name + Random.Range(1, 4).ToString());
					break;
				}

			case animationState.HIGH_CENTER:
				{
					string name = "high center ";
					body.AddForce(transform.up * upForce);
					anim.SetTrigger(name + Random.Range(1, 4).ToString());
					break;
				}

			case animationState.MID_CENTER:
				{
					string name = "mid center ";
					anim.SetTrigger(name + Random.Range(1, 3).ToString());
					break;
				}

			case animationState.LOW_LEFT:
				{
					string name = "low left ";
					body.AddForce(transform.right * goalieForce);
					anim.SetTrigger(name + Random.Range(1, 6).ToString());
					break;
				}

			case animationState.LOW_RIGHT:
				{
					string name = "low right ";
					body.AddForce(-transform.right * goalieForce);
					anim.SetTrigger(name + Random.Range(1, 6).ToString());
					break;
				}

			case animationState.CHEER:
				{
					string name = "cheer ";

					anim.SetTrigger(name + Random.Range(1, 3).ToString());
					break;
				}
		}
	}

	// Update is called once per frame
	void Update()
	{
		if(predetermined)
		{
			//run down the timer THEN play the animation
			if(currentLeapDelay < leapDelay)
			{
				currentLeapDelay += Time.deltaTime;
			}

			else
			{
				currentLeapDelay = 0.0f;
				predetermined = false;
				SetAnimation(determinedState);
				//handler.ChangeState(worldhandler.gameStates.DURING_KICK);
			}
		}
	}

	public void Leap()
	{
		Debug.Log(anim.IsInTransition(0));
		if (!predetermined && anim.GetCurrentAnimatorStateInfo(0).IsName("idle") 
			&& !anim.IsInTransition(0))
		{
			//pick between one of 4 save animations and move the goalie accordingly
			int anim = Random.Range(1, 101);
			if (anim > 0 && anim <= 15)
			{
				SetAnimation(animationState.LOW_RIGHT);
			}

			else if (anim > 15 && anim <= 30)
			{
				SetAnimation(animationState.LOW_LEFT);
			}

			else if (anim > 30 && anim <= 50)
			{
				SetAnimation(animationState.HIGH_CENTER);
			}

			else if (anim > 50 && anim <= 75)
			{
				SetAnimation(animationState.HIGH_RIGHT);
			}

			else if (anim > 75 && anim <= 90)
			{
				SetAnimation(animationState.HIGH_LEFT);
			}

			else
			{
				SetAnimation(animationState.MID_CENTER);
			}
		}

		else
		{
			predetermined = false;
		}
	}

	public void DetermineDirection(animationState animation)
	{
		predetermined = true;
		determinedState = animation;
	}
}
