﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class introCamera : MonoBehaviour {

	public MovieTexture movie;
	float introTimer = 0.0f;

	bool finished = false;
	public delegate void IntroEndEvent();
	public static event IntroEndEvent onIntroEvent;

	public delegate IEnumerator IntroEndCanvasEvent();
	public static event IntroEndCanvasEvent onIntroCanvasEvent;

	AsyncOperation async;

	// Use this for initialization
	void Start () {
		GetComponent<RawImage>().texture = movie as MovieTexture;
		movie.Play();
		StartCoroutine(LoadGame());
	}
	
	// Update is called once per frame
	void Update () {
		if (!finished)
		{

			if (introTimer < movie.duration)
			{
				introTimer += Time.deltaTime;
			}

			else
			{
				GetComponent<RawImage>().enabled = false;
				finished = true;
				ActivateScene();
			}
		}
	}

	IEnumerator LoadGame()
	{
		async = Application.LoadLevelAsync("handler sunny");

		//if (introTimer < movie.duration)
		//{
		async.allowSceneActivation = false;
		yield return async;
		//}
	}

	public void ActivateScene()
	{
		async.allowSceneActivation = true;
	}
}
