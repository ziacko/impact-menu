﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class mainMenu : menuPanel {

	public menuPanel[] menus;
	// Use this for initialization
	void Start()
	{
	}

	// Update is called once per frame
	void Update()
	{
	}

	private void OnDisable()
	{
		BaseOnEnable(false);
	}

	private void OnEnable()
	{
		BaseOnEnable(true);
	}

	public override void BaseOnEnable(bool toEnable)
	{
		base.BaseOnEnable(toEnable);
	}

	public override void ChangePanel(string nextPanel)
	{
		base.ChangePanel(nextPanel);

		switch (nextPanel)
		{
			case "soccer":
				{
					menus[0].enabled = true;
					break;
				}

			case "asteroids":
				{
					menus[1].enabled = true;
					break;
				}

			case "racing":
				{
					menus[2].enabled = true;
					break;
				}

			case "basketball":
				{
					menus[3].enabled = true;
					break;
				}
		}

		//OnDisable myself and enable whatever is picked
		enabled = false;
	}
}
