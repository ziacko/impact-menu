﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class menuPanel : MonoBehaviour {

	public Image backgroundImage;
	//public Text panelName;
	public Image logo;
	public menuPanel parent;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public virtual void BaseOnEnable(bool toEnable)
	{
		backgroundImage.enabled = toEnable;		
		//panelName.gameObject.SetActive(toEnable);
		logo.gameObject.SetActive(toEnable);
	}

	public virtual void ChangePanel(string nextPanel)
	{
		if(nextPanel == "parent")
		{
			parent.enabled = true;
			enabled = false;
		}
	}
}
