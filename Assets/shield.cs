﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class shield : MonoBehaviour {

	// Use this for initialization

	public int health = 5;
	//get all civilians
	//
	public List<civilian> civilians;
	//get a list of civvies
	float opacity = 0.0f;
	public Asteroid.team_t team;

	//each time it takes a hit then remove one

	

	void Start () {
		civilians = new List<civilian>(GetComponentsInChildren<civilian>());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnHit()
	{
		//so when I'm hit knock one off health and take down one of the civs
		civilians[0].Hide(false);
		civilians.RemoveAt(0);
		//Damage();
		health -= 1;

		if(health == 0)
		{
			gameObject.SetActive(false);
		}
	}

	//add opacity
	public void Damage()
	{
		opacity += 0.2f;
		GetComponent<Renderer>().material.SetFloat("_Opacity", opacity);
	}

	void OnCollisionEnter(Collision collision)
	{
		Asteroid aster = collision.gameObject.GetComponent<Asteroid>();
		if (aster != null)
		{
			//if it's the right color AND an asteroid then it's good to go for splosions
			if(aster.team == team)
			{
				Damage();
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{
		Asteroid aster = other.transform.gameObject.GetComponentInChildren<Asteroid>();
		if (aster != null)
		{
			//if it's the right color AND an asteroid then it's good to go for splosions
			if (aster.team == team)
			{
				Damage();
			}
		}
	}
}
