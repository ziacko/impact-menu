﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class soccerBall : MonoBehaviour {

	enum objectHit
	{
		GOALIE = 0,
		GOAL_POST,
		NET,
		MISSED
	}

	public GameObject handlerObject = null;
	public worldhandler_Soccer handler = null;
	public Rigidbody rigid;
	public SphereCollider sphere;
	public const float ballSpeed = 1000.0f;
	const float defaultBallLifeSpan = 2.0f;
	float currentBallLifeSpan = 0.0f;
	Renderer render;
	Material material;

	AudioSource audio;
	bool successful = false;
	bool postHit = false;

	// Use this for initialization
	void Start () {
		handler = GameObject.FindObjectOfType<worldhandler_Soccer>() as worldhandler_Soccer;
		rigid = GetComponent<Rigidbody>();
		sphere = GetComponent<SphereCollider>();
		audio = GetComponent<AudioSource>();
		audio.playOnAwake = false;
		render = GetComponent<Renderer>();
		material = render.sharedMaterial;

		material.globalIlluminationFlags = MaterialGlobalIlluminationFlags.RealtimeEmissive;
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter(Collision col)
	{
		//Debug.Log(col.gameObject.name);
		GetComponent<TrailRenderer>().enabled = false;
		if (col.gameObject.name == "Goalie")
		{
			if (handler.playerAwarded == worldhandler_Soccer.team.kickState_t.NOT_KICKED)
			{
				//successful = false;
				//goalie has caught it
				handler.goalKeeper.caughtBall = false;
				handler.PlaySound(0, handler.sounds.goalFail, 0.5f);
				handler.DisqualifyPlayer();
				render.material.SetColor("_EmissionColor", Color.red);

				//if the ball hist the goalie, look back at the handler's camera
				//and have the ball fly back towards it
				transform.LookAt(handler.gameCameras[handler.currentCamera].transform);
				Ray cameraRay = new Ray(transform.position, transform.forward);
				rigid.AddForce(cameraRay.direction * ballSpeed);
				rigid.velocity = transform.forward;
				Time.timeScale = 0.25f;
			}
		}

		else // just missed everything like a chump
		{
			//successful = false;
			handler.goalKeeper.caughtBall = false;

			if (col.gameObject.name == "net" && !successful)
			{
				postHit = true;
				render.material.SetColor("_EmissionColor", Color.red);
				transform.LookAt(handler.gameCameras[handler.currentCamera].transform);
				Ray cameraRay = new Ray(transform.position, transform.forward);
				rigid.AddForce(cameraRay.direction * ballSpeed);
				rigid.velocity = transform.forward;
				Time.timeScale = 0.5f;
				audio.Play();
			}
		}
	}

	void OnTriggerEnter(Collider col)
	{
		//if the ball has passed the goalie and entered the net 
		//then find the closer side camera and render from there
		//get the distance between the two cameras and see which is closer
		if (handler.currentGameState == worldhandler_Soccer.gameStates.DURING_KICK)
		{
			if (col.gameObject.name == "Bip001 L Hand" || col.gameObject.name == "Bip001 R Hand")
			{
				//successful = false;
				Time.timeScale = 0.5f;
				rigid.useGravity = false;
				rigid.isKinematic = true;
				sphere.enabled = false;
				handler.goalKeeper.caughtBall = true;
				handler.goalKeeper.KeepBallInHands(this);
				//handler.ChangeState(worldhandler.gameStates.POST_KICK);
				handler.PlaySound(0, handler.sounds.goalFail, 0.5f);
				handler.DisqualifyPlayer();
				render.material.SetColor("_EmissionColor", Color.red);
			}

			//ball goes into the net
			else if(col.gameObject.name == "Goal Checker")
			{
				if (!successful && !postHit)
				{
					successful = true;
					Time.timeScale = 0.5f;
					handler.AwardPlayer();
					int newCamera = Random.Range((int)worldhandler_Soccer.cameraPositions.NET_LEFT, handler.gameCameras.Length - 1);
					handler.SwitchCamera(newCamera);
				}
			}
		}
	}
}
