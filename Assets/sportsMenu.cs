﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sportsMenu : menuPanel {

	public Button basketBall;
	public Button soccer;
	public Button tennis;
	public Button boxing;
	public Button gym;
	public Image symbol;

	public Button mainMenu;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnDisable()
	{
		BaseOnEnable(false);
	}

	private void OnEnable()
	{
		BaseOnEnable(true);
	}

	public override void BaseOnEnable(bool toEnable)
	{
		base.BaseOnEnable(toEnable);
		basketBall.gameObject.SetActive(toEnable);
		soccer.gameObject.SetActive(toEnable);
		tennis.gameObject.SetActive(toEnable);
		boxing.gameObject.SetActive(toEnable);
		gym.gameObject.SetActive(toEnable);
		symbol.gameObject.SetActive(toEnable);
		mainMenu.gameObject.SetActive(toEnable);
	}
}
