﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class worldHandler_Basketball : MonoBehaviour {

	// Use this for initialization

	//get some nets and boards
	//a list of balls
	//spawn ball with certain force on click
	//if ball goes through net then give winning player more points
	//override ball callback

	float gameLength = float.PositiveInfinity; //3 minute matches. infinite for now i guess?
	float currentGameTimer = 0.0f;

	board[] boards;

	bool isPaused = false;
	public enum gameState_t
	{
		normal,
		end
	};

	public HUDScore[] scores;

	gameState_t currentGameState;

	Camera gameCamera;

	public GameObject ballPrefab;

	public float ballSpeed = 50.0f;

	GameObject viewTransform;

	AudioSource source;
	public AudioClip music;

	public bool showCursor = false;

	public float inputDelay = 0.5f;

	float currentRightDelay = 0.0f;
	public bool blockRightInput = false;
	
	float currentMidDelay = 0.0f;
	public bool blockMidInput = false;
	
	float currentLeftDelay = 0.0f;
	public bool blockLeftInput = false;

	Cloth[] nets;

	ClothSphereColliderPair[] spherePairs;
	Ending UIEnding;

	public float ResetTimer = 3.0f;
	float currentResetTimer = 0.0f;

	public float ballXScaling = 0.02f;

	public Transform leftSpawnPos;
	public Transform rightSpawnPos;
	public Transform upSpawnPos;
	public Transform downSpawnPos;

	public uint winningScore = 20;

	void Start () {

		boards = GetComponentsInChildren<board>();
		scores = GetComponentsInChildren<HUDScore>();
		nets = GetComponentsInChildren<Cloth>();
		UIEnding = GetComponentInChildren<Ending>();
		spherePairs = new ClothSphereColliderPair[1];
		for (int i = 0; i < 3; i++)
		{
			scores[i].SetColor(board.teamName_t.red + i);
		}
		gameCamera = GetComponentInChildren<Camera>();
		viewTransform = GameObject.Find("LookAt");
		Ball.OnGoal += AwardPlayer;
		source = GetComponent<AudioSource>();
		source.clip = music;
		source.loop = true;
		source.Play();
		Time.timeScale = 1f;
		//UIEnding.Hide(false);
		Cursor.visible = false;
		showCursor = false;
		Reset();
	}
	
	// Update is called once per frame
	void Update () {

		switch (currentGameState)
		{
			case gameState_t.normal:
				{
					if (!isPaused)
					{
						if (Input.GetKeyDown(KeyCode.Space))
						{
							isPaused = true;
						}

						if (Input.GetKeyDown(KeyCode.Return))
						{
							showCursor = !showCursor;
							Cursor.visible = showCursor;
						}

						if (blockRightInput)
						{
							if (currentRightDelay < inputDelay)
							{
								currentRightDelay += Time.deltaTime;
							}

							else
							{
								currentRightDelay = 0.0f;
								blockRightInput = false;
							}
						}

						if (blockMidInput)
						{
							if (currentMidDelay < inputDelay)
							{
								currentMidDelay += Time.deltaTime;
							}

							else
							{
								currentMidDelay = 0.0f;
								blockMidInput = false;
							}
						}

						if (blockLeftInput)
						{
							if (currentLeftDelay < inputDelay)
							{
								currentLeftDelay += Time.deltaTime;
							}

							else
							{
								currentLeftDelay = 0.0f;
								blockLeftInput = false;
							}
						}

						else
						{
							//if (currentGameTimer < gameLength)
							//{
								//currentGameTimer += Time.deltaTime;

								if (Input.GetMouseButtonDown(0))
								{
									RaycastHit hit = new RaycastHit();
									Ray mouseRay = gameCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y + 20, 5));
									//ok so we need to get the point of impact where the mouse was clicked and spawn ball at the point in X axis
									//Vector3 pos = gameCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5f));
									//Debug.Log(pos.ToString());
									if (Physics.Raycast(mouseRay, out hit))
									{

									}

									int screenWidthThird = gameCamera.pixelWidth / 3;

									if(Input.mousePosition.x < screenWidthThird && !blockLeftInput)
									{
										blockLeftInput = true;
										SpawnBall(mouseRay);
									}

									if(Input.mousePosition.x < screenWidthThird * 2 && Input.mousePosition.x > screenWidthThird && !blockMidInput)
									{
										blockMidInput = true;
										SpawnBall(mouseRay);
									}

									if (Input.mousePosition.x > screenWidthThird * 2 && !blockRightInput)
									{
										blockRightInput = true;
										SpawnBall(mouseRay);
									}
								}
							/*}

							else
							{
								ShowWinner();
							}*/
						}
					}

					else
					{
						if(Input.GetKeyDown(KeyCode.Space))
						{
							isPaused = false;
						}
					}
					break;
				}

			case gameState_t.end:
				{
					break;
				}
		}
	}

	void SpawnBall(Ray mouseRay)
	{
		//get how far from the center the mouse position X is
		float midWidth = Screen.width / 2;
		float midHeight = Screen.height / 2;
		//find how far above or how far below the mouse's X position is from the middle
		float diffX =  midWidth - Input.mousePosition.x;
		float diffY = midHeight - Input.mousePosition.y;

		//ok so have 2 transforms being the furthest right and left spawn points for the game
		//lerp between the 2 positions by the diffX. 0 being left and 1 being right.
		//use mousePosX / screen.width
		
		float xPercent = Input.mousePosition.x / Screen.width;
		Vector3 xBallPos = Vector3.Lerp(leftSpawnPos.position, rightSpawnPos.position, xPercent);

		float yPercent = Input.mousePosition.y / Screen.height;
		Vector3 yBallPos = Vector3.Lerp(downSpawnPos.position, upSpawnPos.position, yPercent);

		
		//Vector3 spawnPos = gameCamera.transform.position.x + 
		GameObject newBall = Instantiate(ballPrefab, new Vector3(xBallPos.x, yBallPos.y, mouseRay.origin.z),
			Quaternion.LookRotation(gameCamera.transform.forward, gameCamera.transform.up), gameCamera.transform) as GameObject;
		Debug.Log(newBall.transform.localPosition.ToString());
		//newBall.transform.LookAt(mouseRay.direction);
		newBall.GetComponent<Rigidbody>().AddForce(newBall.transform.forward * ballSpeed, ForceMode.Acceleration);

		spherePairs[0] = new ClothSphereColliderPair(newBall.GetComponent<SphereCollider>());
		foreach (Cloth ballNet in nets)
		{
			ballNet.sphereColliders = spherePairs;
		}
	}

	void ShowWinner(board.teamName_t team)
	{
		/*uint highest = 0;
		int winningPlayer = 0;

		bool draw = false;

		for(int i = 0; i < boards.Length; i++)
		{
			if(boards[i].score > highest)
			{
				winningPlayer = i;
				highest = boards[i].score;
				draw = false;
			}

			else if(boards[i].score == highest)
			{
				draw = true;
			}*/

			UIEnding.Hide(true);
			UIEnding.Set(team);
			currentGameState = gameState_t.end;

			//get every button and turn them off
			//Button[] buttons = GetComponentsInChildren<Button>();

			/*foreach(Button but in buttons)
			{
				but.GetComponent<Image>().enabled = false;
				but.enabled = false;
			}*/

		//UIEnding.SetWinner(team);

		/*if(draw)
		{
			UIEnding.SetWinner(board.teamName_t.none);
		}

		else
		{

		}
	}*/
	}

	void AwardPlayer(board.teamName_t team)
	{
		boards[(int)team].score++;
		scores[(int)team].SetScore(boards[(int)team].score);

		if(boards[(int)team].score == winningScore)
		{
			ShowWinner(team);
		}
	}

	void Reset()
	{
		currentGameState = gameState_t.normal;
		currentGameTimer = 0.0f;
		isPaused = false;
		UIEnding.Hide(false);

		for(int i = 0; i < boards.Length; i++)
		{
			boards[i].score = 0;
			scores[i].SetScore(boards[i].score);
		}

		Button[] buttons = GetComponentsInChildren<Button>();

		foreach (Button but in buttons)
		{
			but.GetComponent<Image>().enabled = true;
			but.enabled = true;
		}
	}

	private void OnDisable()
	{
		Ball.OnGoal -= AwardPlayer;
	}

	public void BackToMain()
	{
		SceneManager.LoadScene("mainScene");
	}
}
