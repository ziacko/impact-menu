﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class worldHandler_Menu : MonoBehaviour {

	public enum menuStages
	{
		main,
		gameType,
		numTeams,
		ageGroup,
		start,
	};

	//get a handle to the GUI handler where each panel is a new menu dealy
	//public mainMenu main;
	public Intro gameIntro;
	public GameObject canvas;

	// Use this for initialization
	void Start () {
		canvas.SetActive(false);
		//gameIntro.player.Play();
		Cursor.visible = false;
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyDown(KeyCode.Return))
		{
			Cursor.visible = !Cursor.visible;
		}
		if (!gameIntro.player.isPlaying)
		{
			canvas.SetActive(true);
			gameIntro.gameObject.SetActive(false);
		}
		
		//ok from here we handle all input.
		//make a switch depending on the name of the mesh collider that was
		//clicked on. or just use buttons with images instead
	}
}
