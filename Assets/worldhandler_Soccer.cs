﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
//using UnityEditor;
using System.Collections;
using System.Globalization;
using UnityEngine.SceneManagement;

public class worldhandler_Soccer : MonoBehaviour
{

	public enum cameraPositions
	{
		//MENU = 0,
		FIRST_PERSON = 0,
		//WIDE_SHOT_LEFT,
		//WIDE_SHOT_RIGHT,
		NET_LEFT,
		NET_RIGHT,
	};

	public enum gameStates
	{
		PLAYER_SETUP = 0,
		PREPARING_KICK,
		DURING_KICK,
		POST_KICK,
		//MENU,
		TEAM_WIN,
		BEGIN,
		END,
		STARTUP,
		SUDDEN_DEATH,
		WIDE_SHOT,
		GOALIE_CHEER,
	};

	public enum teamName_t
	{
		red = 0,
		green,
		blue
	};

	public class team
	{
		public team(/*uint numKicks*/)
		{
			totalScore = 0;
			//currentKick = 0;
			scores = new kickState_t[5] { 0, 0, 0, 0, 0};
		}

		public enum kickState_t
		{
			NOT_KICKED = 0,
			FAILED,
			SUCCESS
		};

		public uint totalScore;
		//public uint currentKick;
		public kickState_t[] scores;
	}

	public teamName_t currentTeam;
	public team[] teams;

	//use this prefab to speed up instantiation
	public GameObject ballPrefab;
	GameObject gameBall;

	public GameCamera[] gameCameras = new GameCamera[7];
	public uint currentCamera;

	public gameStates currentGameState;

	public MainMenu_Soccer menu;

	public float teamWinTimer = 3.0f;
	float currentTeamWinTimer = 0.0f;

	public float playerSetupDelay = 3.0f;
	float currentPlayerSetupDelay = 0.0f;
	
	public float duringKickDelay = 1.0f;
	float currentDuringClickDelay = 0.0f;

	public float postKickDelay = 2.0f;
	float currentPostClickDelay = 0.0f;

	public float beginDelay = 1.5f;
	float currentBeginDelsy = 0.0f;

	public float endDelay = 1.5f;
	public float currentEndDelay = 0.0f;

	public float wideShotDelay = 1.5f;
	float currentWideShotDelay = 0.0f;

	public float cheerDelay = 5f;
	float currentCheerDelay;

	bool ballKicked = false;
	public team.kickState_t playerAwarded = team.kickState_t.NOT_KICKED;

	public bool gamePaused = false;

	int currentTeamIndex = 0;

	public goalie goalKeeper;
	//uint numTeams = 2;
	public uint winningValue = 5;
	HUD currentHUD;
	public GameObject menuObject;

	public SoundManager sounds;
	AudioSource[] source;

	uint currentRound = 0;
	bool isSuddenDeath = false;

	public bool showCursor = true;

	//get the scoreboard and set the GUI elements

	//there can only be one ball in the world at one time

	// Use this for initialization
	void Start()
	{
		currentTeam = teamName_t.red;
		teams = new team[3]{new team(), new team(), new team()};

		currentGameState = gameStates.STARTUP;
		currentHUD = GameObject.Find("HUD").GetComponent<HUD>();
		
		source = GetComponents<AudioSource>();
		sounds = GetComponent<SoundManager>();
	}

	void HideMenu(bool hide)
	{
		//menuObject.SetActive(hide);
		//gameCameras[(uint)cameraPositions.MENU].gameObject.SetActive(hide);
	}

	void PlayMusic()
	{
		source[0].clip = sounds.defaultMusic;
		source[0].Play();
	}

	public void PlaySound(uint sourceIndex, AudioClip sound, float soundSkip = 0.0f, float soundOffset = 0.0f)
	{
		
		source[sourceIndex].clip = sound;
		source[sourceIndex].time = soundSkip;
		source[sourceIndex].Play((ulong)soundOffset * 44100);
	}

	bool CheckRound()
	{
			//if one of them is 20 or above, set the winner and reset
			for(int teamIter = 0; teamIter < 3; teamIter++)
			{
				if(teams[teamIter].totalScore == 100)
				{
					currentHUD.ShowWinner(currentTeam, true);
					ChangeState(worldhandler_Soccer.gameStates.TEAM_WIN);
					return true;
				}
			}

			return false;
	}

	public void ChangeState(gameStates newState)
	{
		currentGameState = newState;
		currentTeamWinTimer = 0.0f;
		currentPlayerSetupDelay = 0.0f;
		currentPostClickDelay = 0.0f;
		currentDuringClickDelay = 0.0f;
		currentEndDelay = 0.0f;
		currentBeginDelsy = 0.0f;
		Time.timeScale = 1.0f;

		switch (newState)
		{
			/*case gameStates.MENU:
				{
					PlayMusic();
					currentHUD.Disable(true);
					int cameraSwitch = Random.Range((int)worldhandler_Soccer.cameraPositions.FIRST_PERSON, gameCameras.Length - 1);
					SwitchCamera(cameraSwitch);
					currentHUD.DisableHUD(false);
					break;
				}*/

			case gameStates.PLAYER_SETUP:
				{
					//play the crowd sounds
					PlaySound(1, sounds.crowdRandom[Random.Range(0, sounds.crowdRandom.Length)]);
					goalKeeper.Reset();
					Destroy(gameBall);
					ballKicked = false;
					if(!isSuddenDeath)
					{
						currentHUD.DisableHUD(true);
					}
					
					currentHUD.UpdateScores(teams[0], teams[1], teams[2]);
					int cameraSwitch = Random.Range((int)worldhandler_Soccer.cameraPositions.NET_LEFT, gameCameras.Length - 1);
					SwitchCamera(cameraSwitch);

					break;
				}

			case gameStates.PREPARING_KICK:
				{
					PlaySound(0, sounds.stadiumAmbient);
					PlaySound(1, sounds.whistleSound);
					playerAwarded = team.kickState_t.NOT_KICKED;
					if (!isSuddenDeath)
					{
						currentHUD.DisableHUD(true);
					}
					currentHUD.UpdateScores(teams[0], teams[1], teams[2]);
					SwitchCamera((int)cameraPositions.FIRST_PERSON);
					break;
				}

			case gameStates.POST_KICK:
				{
					if (CheckRound())
					{
						return;
					}
					SwitchTeam();
					break;
				}

			case gameStates.TEAM_WIN:
				{
					int cameraSwitch = Random.Range((int)worldhandler_Soccer.cameraPositions.NET_LEFT, gameCameras.Length);
					break;
				}

			case gameStates.WIDE_SHOT:
				{
					SwitchCamera(6);
					break;
				}

			case gameStates.GOALIE_CHEER:
				{
					Time.timeScale = 1.0f;
					goalKeeper.SetAnimation(goalie.animationState.CHEER);
					break;
				}

			case gameStates.END:
				{
					SwitchCamera(6);
					break;
				}

			case gameStates.BEGIN:
			case gameStates.DURING_KICK:
			case gameStates.SUDDEN_DEATH:
				{
					break;
				}
		}
	}

	public void SwitchCamera(int cameraIndex)
	{
		currentCamera = (uint)cameraIndex;
		if (gameCameras[currentCamera].gameCamera != null)
		{
			gameCameras[currentCamera].gameCamera.enabled = true;
			gameCameras[currentCamera].GetComponent<AudioListener>().enabled = true;
			Camera.SetupCurrent(gameCameras[currentCamera].gameCamera);

			//for each camera that is not the current camera disable it
			for (uint cameraIter = 0; cameraIter < gameCameras.Length; cameraIter++)
			{
				if (gameCameras[cameraIter] != null && cameraIter != currentCamera)
				{
					gameCameras[cameraIter].Reset();
					gameCameras[cameraIter].gameCamera.enabled = false;
					gameCameras[cameraIter].GetComponent<AudioListener>().enabled = false;
				}
			}
		}
	}

	void SwitchTeam()
	{
		currentTeamIndex = (currentTeamIndex + 1) % 3;
		//use a quick ternary operator
		
		currentTeam = (teamName_t)currentTeamIndex;// (currentTeam == teamName_t.red) ? teamName_t.blue : teamName_t.red;
		currentHUD.SetActiveScores(currentTeam);
	}

	void KickBall()
	{
		if (!ballKicked)
		{
			RaycastHit vHit = new RaycastHit();
			Ray mouseRay = gameCameras[currentCamera].gameCamera.ScreenPointToRay(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 2));
			if (Physics.Raycast(mouseRay, out vHit))
			{

			}

			Debug.DrawRay(mouseRay.origin, mouseRay.direction, Color.red, float.PositiveInfinity);
			Transform point = transform;
			point.LookAt(mouseRay.direction);
			gameBall = Instantiate(ballPrefab, mouseRay.origin, point.rotation) as GameObject;
			gameBall.transform.LookAt(mouseRay.direction);
			gameBall.GetComponent<Rigidbody>().AddForce(mouseRay.direction * soccerBall.ballSpeed);
			ballKicked = true;
			goalKeeper.Leap();
			ChangeState(gameStates.DURING_KICK);
		}
	}

	public void BackToMainMenu()
	{
		SceneManager.LoadScene("mainScene");
	}

	public void AwardPlayer()
	{
		if (playerAwarded == team.kickState_t.NOT_KICKED)
		{
			PlaySound(0, sounds.goalSuccess);

			playerAwarded = team.kickState_t.SUCCESS;

			teams[(int)currentTeam].totalScore += 10;
			currentHUD.UpdateScores(teams[0], teams[1], teams[2]);
		}
	}

	public void DisqualifyPlayer()
	{
		if (playerAwarded == team.kickState_t.NOT_KICKED)
		{
			playerAwarded = team.kickState_t.FAILED;
			ChangeState(worldhandler_Soccer.gameStates.POST_KICK);
		}

		else
		{
			ChangeState(gameStates.GOALIE_CHEER);
		}
	}

	public void Reset()
	{
		//menu.HideObjects(true);
		goalKeeper.Reset();
		isSuddenDeath = false;
		currentRound = 0;
		//Start();
		currentHUD.reset();
		//menu.HideObjects(false);
		playerAwarded = team.kickState_t.NOT_KICKED;
		for(uint iter = 0; iter < teams.Length; iter++)
		{
			teams[iter].totalScore = 0;
			//teams[iter].currentKick = 0;

			for (uint scoreIter = 0; scoreIter < 5; scoreIter++)
			{
				teams[iter].scores[scoreIter] = team.kickState_t.NOT_KICKED;
			}
		}
		currentTeam = teamName_t.red;
		ChangeState(gameStates.WIDE_SHOT);
	}

	// Update is called once per frame
	void Update()
	{
		//if at any point escape key is pressed then pause
		if (!gamePaused)
		{
			if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Space))
			{
				ChangeState(gameStates.WIDE_SHOT);
				gamePaused = true;
				//menu.enabled = false;//			GameObject.Find("Continue").SetActive(false);
				menu.HideContinue(true);
				//menu.HideObjects(true);
			}

			else if (Input.GetKeyDown(KeyCode.Return))
			{
				showCursor = !showCursor;
				Cursor.visible = showCursor;
			}
		}

		if (!gamePaused)
		{
			sounds = GetComponent<SoundManager>();
			switch (currentGameState)
			{
				//at the beginning of the game or if the player ever presses escape
				/*case gameStates.MENU:
					{
						break;
					}*/

				//while the ball is moving through the air. if the ball is in the goal then swap to net cameras
				case gameStates.DURING_KICK:
					{
						//if the player has kicked the ball, just let it fly until it hits something
						//if the ball hasn't done shit then skip ahead
						if (currentDuringClickDelay < duringKickDelay)
						{
							currentDuringClickDelay += Time.deltaTime;

							if (playerAwarded == team.kickState_t.SUCCESS)
							{
								gameCameras[currentCamera].TrackBall(gameBall);
							}
						}

						else
						{
							if(playerAwarded == team.kickState_t.SUCCESS)
							{
								ChangeState(worldhandler_Soccer.gameStates.POST_KICK);
							}

							else
							{
								DisqualifyPlayer();
							}
						}

						break;
					}

				//back to wide shot and display totalScore.
				case gameStates.POST_KICK:
					{
						//after the ball has either hit or missed the goal.
						//move camera back to the wideshot. randomize?

						if (currentPostClickDelay < postKickDelay)
						{
							currentPostClickDelay += Time.deltaTime;
							if (goalKeeper.caughtBall)
							{
								if (ballPrefab != null)
								{
									Vector3 midpoint = Vector3.Lerp(goalKeeper.leftHand.transform.position, goalKeeper.rightHand.transform.position, 0.5f);
									gameBall.transform.position = midpoint;
								}
							}
						}

						else
						{
							ChangeState(gameStates.PLAYER_SETUP);
						}

						break;
					}

				//this is when the player is gearing up for the kick
				case gameStates.PREPARING_KICK:
					{
						if(Input.anyKeyDown)
						{
							DetermineGoalie();
						}

						if (Input.GetMouseButtonDown(0))
						{
							//if the ball hasn't already been kicked then fire away
							KickBall();
						}
						break;
					}

				//after post kick, we show a wide shot of the goal
				//this is to make sure players have enough time to get into position
				case gameStates.PLAYER_SETUP:
					{

						//just run down the timer then move to prepare kick
						if (currentPlayerSetupDelay < playerSetupDelay)
						{
							currentPlayerSetupDelay += Time.deltaTime;
						}
						else
						{
							ChangeState(gameStates.PREPARING_KICK);
						}
						break;
					}

				case gameStates.TEAM_WIN:
					{
						
						if (currentTeamWinTimer < teamWinTimer)
						{
							currentTeamWinTimer += Time.deltaTime;
						}

						else
						{
							ChangeState(gameStates.END);
						}
						break;
					}

				case gameStates.BEGIN:
					{
						if(currentBeginDelsy < beginDelay)
						{
							currentBeginDelsy += Time.deltaTime;
						}

						else
						{
							ChangeState(gameStates.PLAYER_SETUP);
						}
						break;
					}

				case gameStates.END:
					{
						/*if(currentEndDelay < endDelay)
						{
							currentEndDelay += Time.deltaTime;
						}

						else
						{
							Reset();
							//menu.HideObjects(true);
						}*/
						break;
					}

				case gameStates.STARTUP:
					{
						//when startup ends
						//menu.HideIntro(false);
						//menu.HideObjects(true);
						PlayMusic();
						ChangeState(gameStates.WIDE_SHOT);
						break;
					}

				case gameStates.WIDE_SHOT:
					{
						if(currentWideShotDelay < wideShotDelay)
						{
							currentWideShotDelay += Time.deltaTime;
						}

						else
						{
							currentWideShotDelay = 0.0f;
							currentHUD.SetActiveScores(teamName_t.red);
							ChangeState(gameStates.BEGIN);
						}

						break;
					}

				case gameStates.GOALIE_CHEER:
					{
						Time.timeScale = 1.0f;
						if(currentCheerDelay < cheerDelay)
						{
							currentCheerDelay += Time.deltaTime;
						}

						else
						{
							currentCheerDelay = 0.0f;
							ChangeState(gameStates.PLAYER_SETUP);
						}
						break;
					}
			}
		}
	}

	void DetermineGoalie()
	{
		if (Input.GetKeyDown(KeyCode.W) || Input.GetKeyDown(KeyCode.Keypad8))
		{
			goalKeeper.DetermineDirection(goalie.animationState.HIGH_CENTER);
		}
		
		else if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.Keypad4))
		{
			goalKeeper.DetermineDirection(goalie.animationState.LOW_LEFT);
		}

		else if(Input.GetKeyDown(KeyCode.S) || Input.GetKeyDown(KeyCode.Keypad5))
		{
			goalKeeper.DetermineDirection(goalie.animationState.MID_CENTER);
		}

		else if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.Keypad6))
		{
			goalKeeper.DetermineDirection(goalie.animationState.LOW_RIGHT);
		}

		else if (Input.GetKeyDown(KeyCode.Q) || Input.GetKeyDown(KeyCode.Keypad7))
		{
			goalKeeper.DetermineDirection(goalie.animationState.HIGH_LEFT);
		}

		else if (Input.GetKeyDown(KeyCode.E) || Input.GetKeyDown(KeyCode.Keypad9))
		{
			goalKeeper.DetermineDirection(goalie.animationState.HIGH_RIGHT);
		}
	}

	public void UnPause()
	{
		gamePaused = true;
		//get the continue button in the menu and hide it
		//then hide the rest of the menu
		
		ChangeState(gameStates.PLAYER_SETUP);
	}
}
